<?php
if (isset($_POST['submit'])) {
    $to = "contacto@elaaa.com.mx"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $nombre = $_POST['nombre'];
    $telefono = $_POST['telefono'];
    $mensaje = $_POST['mensaje'];
    $subject = "Información ELAAA";
    $headers = "From:" . $from . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8";

    $body = '<html><body>';
    $body .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    $body .= "<tr style='background: #eee;'><td><strong>Nombre:</strong> </td><td>" . $nombre . "</td></tr>";
    $body .= "<tr><td><strong>Email:</strong> </td><td>" . $from . "</td></tr>";
    if (($telefono) != '') {
        $body .= "<tr><td><strong>Teléfono:</strong> </td><td>" . $telefono . "</td></tr>";
    }
    $body .= "<tr><td><strong>Mensaje:</strong> </td><td>" . $mensaje . "</td></tr>";
    $body .= "</table>";

    mail($to, $subject, $body, $headers);
    header('Location: email.html'); // You can also use to redirect to another page.
}
